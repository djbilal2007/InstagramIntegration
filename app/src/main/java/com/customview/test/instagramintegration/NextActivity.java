package com.customview.test.instagramintegration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class NextActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        Intent intent = getIntent();

        String fullname = intent.getStringExtra("fullname");
        String username = intent.getStringExtra("username");
        String imageUrl = intent.getStringExtra("imageUrl");


        ImageView imageView = (ImageView)findViewById(R.id.imageView);
        TextView fullname_txt = (TextView)findViewById(R.id.fullname);
        TextView username_txt = (TextView)findViewById(R.id.username);

        fullname_txt.setText(fullname);
        username_txt.setText(username);

        Picasso.with(this)
                .load(imageUrl)
                .resize(600,600)
                .into(imageView);
    }
}
