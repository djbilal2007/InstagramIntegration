package com.customview.test.instagramintegration;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Instagram";

    private static final String AUTHURL = "https://api.instagram.com/oauth/authorize/";
    private static final String TOKENURL = "https://api.instagram.com/oauth/access_token";
    private static final String APIURL = "https://api.instagram.com/v1";
    private static final String CALLBACKURL = "http://www.ratufa.com";

    private static String request_token;
    private String tokenURLString;
    private String authURLString;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        authURLString = AUTHURL + "?client_id=" + context.getResources().getString(R.string.client_id) + "&redirect_uri=" + CALLBACKURL + "&response_type=code&display=touch&scope=likes+comments+relationships";
        tokenURLString = TOKENURL + "?client_id=" + context.getResources().getString(R.string.client_id) + "&client_secret=" + context.getResources().getString(R.string.client_secret) + "&redirect_uri=" + CALLBACKURL + "&grant_type=authorization_code";

        Log.i("URL", authURLString);
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("Login into Instagram");

        LinearLayout linearLayout = new LinearLayout(MainActivity.this);
        WebView webView = new WebView(MainActivity.this);
        EditText editText = new EditText(MainActivity.this);

        editText.setVisibility(View.GONE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setWebViewClient(new AuthWebViewClient());
        webView.loadUrl(authURLString);

        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(webView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.addView(editText, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        alert.setView(linearLayout);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    //    alert.create().show();
    }

    private class AuthWebViewClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(url.startsWith(CALLBACKURL)){
                Log.i(TAG, url);
                String [] parts = url.split("=");
                request_token = parts[1];
                Log.d(TAG, request_token);
                new MyAsyncTask().execute();
                return true;
            }
            return false;
        }
    }

    private class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                URL url = new URL(tokenURLString);
                Log.d(TAG, "Url: " + url.toString());
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                httpsURLConnection.setRequestMethod("POST");
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpsURLConnection.getOutputStream());
                outputStreamWriter.write("client_id=" + context.getResources().getString(R.string.client_id)
                        + "&client_secret=" + context.getResources().getString(R.string.client_secret)
                        + "&grant_type=authorization_code"
                        + "&redirect_uri=" + CALLBACKURL
                        + "&code=" + request_token);

                outputStreamWriter.flush();
                String response = streamToString(httpsURLConnection.getInputStream());

                Log.i(TAG,"Response: " + response);

                JSONObject jsonObject = new JSONObject(response);
                String access_token = jsonObject.getString("access_token");
                JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                String full_name = jsonObject1.getString("full_name");
                String username = jsonObject1.getString("username");
                String profileImageUrl = jsonObject1.getString("profile_picture");

                Log.d(TAG, "Access Token: " + access_token);
                Log.d(TAG, "Full Name: " + full_name);
                Log.d(TAG, "Username: " + username);
                Log.d(TAG, "Profile Image URL: " + profileImageUrl);

                Intent intent = new Intent(MainActivity.this, NextActivity.class);
                intent.putExtra("fullname", full_name);
                intent.putExtra("username", username);
                intent.putExtra("imageUrl", profileImageUrl);
                startActivity(intent);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static String streamToString(InputStream inputStream) {
        try{
            BufferedReader br;
            StringBuffer sbuffer = new StringBuffer();
            br = new BufferedReader(new InputStreamReader(inputStream));
            String str = br.readLine();

            while(str != null){
                sbuffer.append(str);
                str = br.readLine();
            }

            Log.d("Sbuffer", sbuffer.toString());
            return sbuffer.toString();


        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
